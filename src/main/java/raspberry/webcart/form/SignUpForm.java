package raspberry.webcart.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by sasho on 13.01.2018.
 */
public class SignUpForm {

    @NotEmpty
    @Size(min = 4, max = 30)
    private String firstName = "";

    @NotEmpty
    @Size(min = 4, max = 30)
    private String lastName = "";

    @NotEmpty
    @Size(min = 10, max = 45)
    @Pattern(regexp = "^[_A-Za-z0-9-\\\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
    private String email = "";

    @NotEmpty
    @Size(min = 4, max = 30)
    private String username = "";

    @NotEmpty
    @Size(min = 8, max = 30)
    @Pattern(regexp = "(?=^.{8,}$)(?=.*[0-9a-z0-9]).*$")
    private String password = "";

    @NotEmpty
    @Pattern(regexp = "(?=^.{8,}$)(?=.*[0-9a-z0-9]).*$")
    @Size(min=8, max=30)
    private String passwordCheck = "";

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordCheck() {
        return passwordCheck;
    }

    public void setPasswordCheck(String passwordCheck) {
        this.passwordCheck = passwordCheck;
    }
}
