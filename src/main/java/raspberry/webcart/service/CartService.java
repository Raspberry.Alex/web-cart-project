package raspberry.webcart.service;

import raspberry.webcart.model.Cart;
import raspberry.webcart.model.Product;

import java.util.List;

/**
 * Created by sasho on 13.01.2018.
 */
public interface CartService {

    Cart findCartById(Long cartId);

    Cart addCart(Cart cart);

    void deleteCartById(Long cartId);

    Cart edit(Cart cart);

    Cart getCartByUserId(Long userId);

    Product addProduct(Product product);

    Product removeProduct(Product product);

    Product removeProductById(Long productId);

}
