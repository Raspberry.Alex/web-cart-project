package raspberry.webcart.service;

import raspberry.webcart.model.User;

import java.util.List;

/**
 * Created by sasho on 13.01.2018.
 */
public interface UserService {

    List<User> findAll();

    User findById(Long id);

    User findByUsername(String username);

    User create(User user);

    User edit(User user);

    void deleteById(Long userId);

}
