package raspberry.webcart.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import raspberry.webcart.model.Product;
import raspberry.webcart.repository.ProductRepository;

import javax.xml.stream.util.EventReaderDelegate;
import java.util.List;

/**
 * Created by sasho on 13.01.2018.
 */
@Service
@Primary
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;


    @Override
    public List<Product> findAll() {
        return this.productRepository.findAll();
    }

    @Override
    public Product findProductById(Long productId) {
        return this.productRepository.findProductById(productId);
    }

    @Override
    public List<Product> findProductsByName(String productName) {
        return this.productRepository.findProductsByName(productName);
    }

    @Override
    public Product save(Product product) {
        return this.productRepository.save(product);
    }

    @Override
    public void deleteById(Long id) {
        this.productRepository.deleteProductById(id);
    }

    @Override
    public Product findById(Long id) {
        return this.productRepository.findProductById(id);
    }

    @Override
    public Product edit(Product product) {
        return this.productRepository.save(product);
    }
}
