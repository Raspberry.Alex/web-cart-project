package raspberry.webcart.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import raspberry.webcart.model.Cart;
import raspberry.webcart.model.Product;
import raspberry.webcart.repository.CartRepository;

import java.util.List;

/**
 * Created by sasho on 13.01.2018.
 */
@Service
@Primary
public class CartServiceImpl implements CartService{

    @Autowired
    private CartRepository cartRepository;


    @Override
    public Cart findCartById(Long cartId) {
        return this.cartRepository.getCartById(cartId);
    }

    @Override
    public Cart addCart(Cart cart) {
        return this.cartRepository.save(cart);
    }

    @Override
    public void deleteCartById(Long cartId) {
        this.cartRepository.delete(cartId);
    }

    @Override
    public Cart edit(Cart cart) {
        return this.cartRepository.edit(cart);
    }

    @Override
    public Cart getCartByUserId(Long userId) {
        return cartRepository.getCartByUserId(userId);
    }

    @Override
    public Product addProduct(Product product) {
        return cartRepository.addProduct(product);
    }

    @Override
    public Product removeProduct(Product product) {
        return cartRepository.removeProduct(product);
    }

    @Override
    public Product removeProductById(Long productId) {
        return cartRepository.removeProductById(productId);
    }


}
