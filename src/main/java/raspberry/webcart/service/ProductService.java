package raspberry.webcart.service;

import raspberry.webcart.model.Product;

import java.util.List;

/**
 * Created by sasho on 13.01.2018.
 */
public interface ProductService {

    List<Product> findAll();

    Product findProductById(Long productId);

    List<Product> findProductsByName(String productName);

    Product save(Product product);

    void deleteById(Long id);

    Product findById(Long id);

    Product edit(Product product);

}
