package raspberry.webcart.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import raspberry.webcart.model.User;
import raspberry.webcart.repository.CartRepository;
import raspberry.webcart.repository.UserRepository;

import java.util.List;

/**
 * Created by sasho on 13.01.2018.
 */
@Service
@Primary
public class UserServiceImpl implements UserService{


    @Autowired
    private UserRepository userRepository;


    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    @Override
    public User findById(Long id) {
        return this.userRepository.findById(id);
    }

    @Override
    public User findByUsername(String username) {
        return this.userRepository.findByUserName(username);
    }

    @Override
    public User create(User user) {
        return this.userRepository.create(user);
    }

    @Override
    public User edit(User user) {
        return this.userRepository.create(user);
    }

    @Override
    public void deleteById(Long userId) {
        this.userRepository.deleteById(userId);
    }
}
