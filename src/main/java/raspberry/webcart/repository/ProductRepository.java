package raspberry.webcart.repository;

import com.sun.tracing.dtrace.ModuleAttributes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import raspberry.webcart.model.Product;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by sasho on 13.01.2018.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    public Product findProductById(Long productId);

    public Product findProductByName(String productName);

    public List<Product> findProductsByName(String productName);

    public Product save(Product product);

    @Query("delete from Product p where p.id = :productId")
    @Transactional
    @Modifying
    public void deleteProductById(Long productId);
}
