package raspberry.webcart.repository;

import org.springframework.stereotype.Repository;
import raspberry.webcart.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by sasho on 13.01.2018.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>{

    User findById(Long id);

     User findByUserName(String userName);

    User create(User user);

    @Query("select count(u.id) from User u")
     int findNumberOfUsers();

    @Query("select u.id from User u order by u.id asc")
     long[] findUserIds();

    void deleteById(Long id);
}
