package raspberry.webcart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import raspberry.webcart.model.Cart;
import raspberry.webcart.model.Product;

import java.util.List;

/**
 * Created by sasho on 13.01.2018.
 */
@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

    Cart getCartById(Long cartId);

    Cart save(Cart cart);

    Cart edit(Cart cart);

    void delete(Long cartId);

    Cart getCartByUserId(Long userId);

    Product addProduct(Product product);

    Product removeProduct(Product product);

    Product removeProductById(Long productId);

    List<Product> getAllProductsByUserId(Long userId);
}
