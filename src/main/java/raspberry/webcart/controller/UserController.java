package raspberry.webcart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import raspberry.webcart.form.SignUpForm;
import raspberry.webcart.model.Cart;
import raspberry.webcart.model.Product;
import raspberry.webcart.model.User;
import raspberry.webcart.service.CartService;
import raspberry.webcart.service.ProductService;
import raspberry.webcart.service.UserService;

import org.springframework.security.core.Authentication;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by sasho on 13.01.2018.
 */
@Controller
public class UserController {

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @Autowired
    private CartService cartService;

    @RequestMapping("/login")
    public String login(){
        return "/users/login";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup(Model model){
        model.addAttribute("signupForm", new SignUpForm());
        return "users/signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signupUser(@Valid @ModelAttribute("signupForm") SignUpForm signUpForm,
                             BindingResult bindingResult){
        if(!bindingResult.hasErrors()){
            if(signUpForm.getPassword().equals(signUpForm.getPasswordCheck())){

                User newUser = new User();
                newUser.setFirstName(signUpForm.getFirstName());
                newUser.setLastName(signUpForm.getLastName());
                newUser.setUsername(signUpForm.getUsername());
                newUser.setEmail(signUpForm.getEmail());
                newUser.setPassword(signUpForm.getPassword());

                Cart cart = new Cart();
                newUser.setCart(cart);

                if(userService.findByUsername(signUpForm.getUsername()) == null){
                    userService.create(newUser);
                } else {
                    bindingResult.rejectValue("username", "error.userexists", "Username already exists!");
                            return "/users/signup";
                }

            } else {
                bindingResult.rejectValue("passwordCheck", "error.pwdmach", "Passwords does not match!");
                return "/users/signup";
            }
        } else {
            return "/users/signup";
        }
        return "redirect:/login";
    }

//    @RequestMapping("/products/all-products")
//    public String allProducts(Model model){
//        String currentUserName = "";
//
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if(!(authentication instanceof AnonymousAuthenticationToken)){
//            currentUserName = authentication.getName();
//            User users = userService.findByUsername(currentUserName);
//            model.addAttribute("currentUser", users);
//        }
//        List<Product> allProducts = productService.findAll();
//        model.addAttribute("allProducts", allProducts);
//
//        return "/products/all-products";
//    }

    @RequestMapping(value = "/create-user", method = RequestMethod.POST)
    public String createUser(String firstName, String lastName, String username, String password,
                             String email){
        User user = null;
        try{
            user = new User(firstName, lastName, username, password, email, new Cart());
            userService.create(user);
        } catch(Exception e){
            e.printStackTrace();
            return "redirect:/general-error?msg=" +
                    "Error creating users: " + e.toString();
        }

        return "redirect:/products/all-products";
    }

    @RequestMapping(value = "/update-user", method = RequestMethod.GET)
    public String updateUser(Long id, String firstName, String lastName, String username, String email){

        try{
            User user = userService.findById(id);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setUsername(username);
            user.setEmail(email);

            userService.create(user);
        } catch(Exception e){
            e.printStackTrace();
            return "redirect:/general-error?msg=" +
                    "Error updating users: " + e.toString();
        }
        return "redirect:/products/all-products";
    }



}
