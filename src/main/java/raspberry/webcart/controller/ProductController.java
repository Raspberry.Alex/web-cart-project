package raspberry.webcart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import raspberry.webcart.model.Product;
import raspberry.webcart.model.User;
import raspberry.webcart.service.CartService;
import raspberry.webcart.service.ProductService;
import raspberry.webcart.service.UserService;

import java.util.List;

/**
 * Created by sasho on 14.01.2018.
 */

@Controller
public class ProductController {

    @Autowired
    private UserService userService;

    @Autowired
    private CartService cartService;

    @Autowired
    private ProductService productService;


    @RequestMapping(value = "products/all-products")
    public String products(Model model){

        String currentUserName = "";

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!(authentication instanceof AnonymousAuthenticationToken)){
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            model.addAttribute("currentUser", user);
        }

        List<Product> products = productService.findAll();

        model.addAttribute("allProducts", products);

        return "/products/all-products";
    }

    @RequestMapping(value = "/buy-Product/{id}")
    public String buyProduct(@PathVariable("id") long id) {

        Product product = productService.findProductById(id);

        this.cartService.addProduct(product);

        return "/products/all-products";
    }
}
